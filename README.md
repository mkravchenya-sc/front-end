# Library

The application is an online library.
To run the application, follow these steps:
1. In the command line from the root directory of the application, run the command "npm start"
2. Go to [http://localhost:3000](http://localhost:3000/)
