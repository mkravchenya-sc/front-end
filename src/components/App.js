import React, { PureComponent } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Home from './pages/Home';
import Books from './pages/Books';
import 'bootstrap/dist/css/bootstrap.css';
import './style.css';
import './table.css';

export default class App extends PureComponent {
  render() {
    document.body.classList.add('text-center');
    document.getElementById('root').classList.add('h-100');
    return (
      <Router>
        <Switch>
          <Route exact={true} path="/" component={Home} />
          <Route exact={true} path="/books" component={Books} />
        </Switch>
      </Router>
    )
  }
}
