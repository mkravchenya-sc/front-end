import React, { PureComponent } from 'react';

export default class Footer extends PureComponent {
  render() {
    return (
      <footer className="mastfoot mt-auto">
        <div className="inner mx-auto">
          <p>
            Site developed by <a href="/">Mikhail Kravchenya</a>.
          </p>
        </div>
      </footer>
    )
  }
}
