import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Navbar from './Navbar';

export default class Header extends PureComponent {
  static propTypes = {
    active: PropTypes.string
  }

  render() {
    console.log(this.props['active']);
    return (
      <header className="masthead sticky-top">
        <div className="inner mx-auto">
          <Navbar active={this.props['active']}/>
        </div>
      </header>
    )
  }
}
