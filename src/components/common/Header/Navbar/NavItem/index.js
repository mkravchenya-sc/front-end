import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

export default class NavItem extends PureComponent {
  static propTypes = {
    href: PropTypes.string,
    link: PropTypes.string,
    isActive: PropTypes.bool
  }

  render() {
    const { href, link, isActive } = this.props;
    const active = (
      <li className="nav-item">
        <a className="nav-link active" href={href}>{link}</a>
      </li>
    )
    const notActive = (
      <li className="nav-item">
        <a className="nav-link" href={href}>{link}</a>
      </li>
    )
    console.log('item: ' + isActive);
    return isActive ? active : notActive;
  }
}
