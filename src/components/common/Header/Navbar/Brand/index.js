import React, { PureComponent } from 'react';
import logo from './logo.png';

export default class Brand extends PureComponent {
  render() {
    return (
      <a className="navbar-brand" href="/">
        <img className="logo-img" alt="Logo" src={logo}/>
      </a>
    )
  }
}
