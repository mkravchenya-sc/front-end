import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Brand from './Brand';
import NavItem from './NavItem';

export default class Navbar extends PureComponent {
  static propTypes = {
    active: PropTypes.string
  }

  render() {
    console.log('navbar: ' + this.props['active']);
    const checkActive = (link) => this.props['active'] === link ? true : false

    console.log(checkActive('books'));

    return (
      <nav className="navbar navbar-expand-lg">
        <div className="collapse navbar-collapse" id="navbarTogglerDemo01">
          <Brand/>
          <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
            <NavItem link="HOME" href="/" isActive={checkActive('home')}/>
            <NavItem link="BOOKS" href="/books" isActive={checkActive('books')}/>
          </ul>
        </div>
      </nav>
    )
  }
}
