import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

export default class Book extends PureComponent {
  static propTypes = {
    book: PropTypes.object,
    bookIndex: PropTypes.number
  }

  state = {
    isSelected: false
  }

  handleSelect = () => {
    if (this.state.isSelected) {
      this.setState({ isSelected: false });
    } else {
      this.setState({ isSelected: true });
    }
  }

  showBook = () => {
    const { book, bookIndex } = this.props;
    return (
      <div>
        <th scope="row">{bookIndex + 1}</th>
        <td>{book.title}</td>
        <td>{book.author}</td>
      </div>
    )
  }

  render() {
    const { book, bookIndex } = this.props;
    const selected = (
      <tr id="data" onClick={() => this.handleSelect()} className="selected">
        <th scope="row">{bookIndex + 1}</th>
        <td>{book.title}</td>
        <td>{book.author}</td>
      </tr>
    )
    const notSelected = (
      <tr key={book.id} id="data" onClick={() => this.handleSelect()}>
        <th scope="row">{bookIndex + 1}</th>
        <td>{book.title}</td>
        <td>{book.author}</td>
      </tr>
    )
    return this.state.isSelected ? selected : notSelected;
  }
}
