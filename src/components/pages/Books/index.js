import React, { PureComponent } from 'react';
import Header from '../../common/Header';
import Footer from '../../common/Footer';
import books from '../../../resources/data.js';
import Book from './Book';

export default class Books extends PureComponent {
  state = {
    displayedBooks: books,
    sortedField: null,
    isAscSortDirection: false
  }

  handleSearch = (event) => {
    const searchQuery = event.target.value.toLowerCase();
    const displayedBooks = books.filter(book =>
      book.title.toLowerCase().indexOf(searchQuery) !== -1 ||
      book.author.toLowerCase().indexOf(searchQuery) !== -1)

    this.setState({
      displayedBooks: displayedBooks,
      sortedField: this.state.sortedField,
      isAscSortDirection: this.state.isAscSortDirection
    })
  }

  compareBy(key) {
    const isAscSortDirection = this.state.isAscSortDirection;
    return function(a, b) {
      const result = a[key] < b[key] ? 1 : (a[key] > b[key] ? -1 : 0);
      return isAscSortDirection ? result : -result;
    };
  }

  sortBy(key) {
    if (this.state.sortedField !== key) {
      this.setState({
        displayedBooks: this.state.displayedBooks,
        sortedField: key,
        isAscSortDirection: true
      })
    } else {
      this.setState({
        displayedBooks: this.state.displayedBooks,
        sortedField: key,
        isAscSortDirection: this.state.isAscSortDirection === true ? false : true
      })
    }
    const arrayCopy = [...this.state.displayedBooks];
    arrayCopy.sort(this.compareBy(key));
    this.setState({ displayedBooks: arrayCopy });
  }

  render() {
    const showBooks = this.state.displayedBooks.map((book, index) =>
      <Book key = {book.id} book = {book} bookIndex = {index}/>
    )

    return (
      <div className="cover-container d-flex w-100 h-100 flex-column">
        <Header active="books"/>
        <main role="main" className="justify-content-start">
          <div className="inner mx-auto">
            <div id="booksTable">
              <form className="form-inline my-2 my-lg-0">
                <input className="form-control mr-sm-2" id="search-book"
                  placeholder="Table search" aria-label="Search"
                  onChange={this.handleSearch}
                />
              </form>
              <table id="books" className="table tablesorter">
                <thead className="thead-dark">
                  <tr id="row">
                    <th scope="col">#</th>
                    <th scope="col" onClick={() => this.sortBy('title')}>Title</th>
                    <th scope="col" onClick={() => this.sortBy('author')}>Author</th>
                  </tr>
                </thead>
                <tbody>
                  {showBooks}
                </tbody>
              </table>
            </div>
          </div>
        </main>
        <Footer/>
      </div>

    )
  }
}
