export default [
  {
    "id": 1,
    "title": "JavaScript The Definitive Guide, 5th Edition",
    "author": "David Flanagan"
  },
  {
    "id": 2,
    "title": "You Don't Know JS (book series)",
    "author": "Kyle Simpson"
  },
  {
    "id": 3,
    "title": "Thinking in Java",
    "author": "Bruce Eckel"
  },
  {
    "id": 4,
    "title": "Effective Java",
    "author": "Joshua Bloch"
  },
  {
    "id": 5,
    "title": "Clean Code",
    "author": "Robert C. Martin"
  },
  {
    "id": 6,
    "title": "Spring in Action",
    "author": "Craig Walls"
  },
  {
    "id": 7,
    /* eslint-disable no-script-url */
    "title": "JavaScript: The Good Parts",
    "author": "Douglas Crockford"
  }
]
